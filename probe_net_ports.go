package main

import (
	"strconv"

	"github.com/golang/glog"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/procfs"
)

type ProbeListeningTCPPorts struct {
	listening_ports_start                      []int
	listening_ports                            []int
	inodes                                     []int
	metric_total_listening_tcp_ports           prometheus.Gauge
	metric_new_listening_tcp_ports_since_start prometheus.Gauge
}

func (probe *ProbeListeningTCPPorts) Fetch() {
	fs, err := procfs.NewDefaultFS()
	if err != nil {
		glog.Fatal(err)
	}
	tcp, err := fs.NetTCP()
	if err != nil {
		glog.Fatal(err)
	}

	var listening_ports []int
	var inodes []int

	for _, port := range tcp {
		// 10 is LISTEN state
		if port.St == 10 {
			listening_ports = append(listening_ports, int(port.LocalPort))
			inodes = append(inodes, int(port.Inode))
		}
	}
	probe.listening_ports = listening_ports
	probe.inodes = inodes
	if probe.listening_ports_start == nil {
		probe.listening_ports_start = listening_ports
	}
}

func (probe *ProbeListeningTCPPorts) Update() {
	old_ports := probe.listening_ports
	probe.Fetch()
	new_ports := probe.listening_ports

	probe.metric_total_listening_tcp_ports.Set(float64(len(new_ports)))
	probe.metric_new_listening_tcp_ports_since_start.Set(float64(len(new_ports) - len(probe.listening_ports_start)))

	for _, new_port := range new_ports {
		found := false
		for _, old_port := range old_ports {
			if new_port == old_port {
				found = true
				break
			}
		}

		if !found {
			glog.Warning("port", new_port, "was opened")
		}
	}
}

func GetPidsFromInodes(open_ports_inodes []int) {
	fs, err := procfs.NewDefaultFS()
	if err != nil {
		glog.Fatal(err)
	}
	procs, err := fs.AllProcs()
	if err != nil {
		glog.Fatal(err)
	}

	// For each running process
	for _, proc := range procs {
		proc_fds, err := proc.FileDescriptorTargets()
		if err != nil {
			glog.Fatal(err)
		}

		// For each file descriptor of selected process
		for _, proc_fd := range proc_fds {

			// For each inode that's taken by an open port
			for _, port_inode := range open_ports_inodes {
				if proc_fd == "socket:["+strconv.Itoa(port_inode)+"]" {
					proc_name, err := proc.Comm()
					if err != nil {
						glog.Fatal(err)
					}

					glog.Info("proc", proc.PID, proc_name, "has open fd", proc_fd)
				}
			}
		}
	}
}

func (probe *ProbeListeningTCPPorts) Init() {
	probe.listening_ports_start = nil
	probe.metric_total_listening_tcp_ports = prometheus.NewGauge(prometheus.GaugeOpts{Name: "doggo_metric_total_listening_tcp_ports"})
	probe.metric_new_listening_tcp_ports_since_start = prometheus.NewGauge(prometheus.GaugeOpts{Name: "doggo_metric_new_listening_tcp_ports_since_start"})
}
