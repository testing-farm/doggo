//go:build linux
// +build linux

package main

import (
	"github.com/cilium/ebpf/link"
	"github.com/cilium/ebpf/rlimit"
	"github.com/golang/glog"
	"github.com/prometheus/client_golang/prometheus"
)

// $BPF_CLANG and $BPF_CFLAGS are set by the Makefile.
//go:generate go run github.com/cilium/ebpf/cmd/bpf2go -cc $BPF_CLANG -cflags $BPF_CFLAGS bpf probe_ebpf.c -- -I../headers

const mapKey uint32 = 0

type ProbeKernelModule struct {
	objs                                 *bpfObjects
	fn                                   string
	metric_times_sys_finit_module_called prometheus.Gauge
}

func (probe *ProbeKernelModule) Init() {

	// Name of the kernel function to trace.
	probe.fn = "sys_finit_module"

	// Allow the current process to lock memory for eBPF resources.
	if err := rlimit.RemoveMemlock(); err != nil {
		glog.Fatal(err)
	}

	// Load pre-compiled programs and maps into the kernel.
	probe.objs = &bpfObjects{}
	if err := loadBpfObjects(probe.objs, nil); err != nil {
		glog.Fatalf("loading objects: %v", err)
	}

	// Open a Kprobe at the entry point of the kernel function and attach the
	// pre-compiled program. Each time the kernel function enters, the program
	// will increment the execution counter by 1. The read loop below polls this
	// map value once per second.
	_, err := link.Kprobe(probe.fn, probe.objs.KprobeFinitModule)
	if err != nil {
		glog.Fatalf("opening kprobe: %s", err)
	}

	probe.metric_times_sys_finit_module_called = prometheus.NewGauge(prometheus.GaugeOpts{Name: "doggo_metric_times_sys_finit_module_called"})
}

func (probe *ProbeKernelModule) Update() {

	var value uint64
	if err := probe.objs.KprobeMap.Lookup(mapKey, &value); err != nil {
		glog.Fatalf("reading map: %v", err)
	}
	if value > 0 {
		glog.Warningln(probe.fn, "called", value, "times\n")
	}
	glog.Infof("%s called %d times\n", probe.fn, value)
	probe.metric_times_sys_finit_module_called.Set(float64(value))
}

func (probe *ProbeKernelModule) Destroy() {
	glog.Info("Destroying")
	probe.objs.Close()
}
