package main

import (
	"github.com/golang/glog"
	"github.com/google/gopacket/layers"
	"github.com/prometheus/client_golang/prometheus"
)

// TCPHandshake contains 3 datagrams of a TCP 3-way handshake
type TCPHandshake struct {
	part1 *layers.TCP
	part2 *layers.TCP
	part3 *layers.TCP
}

type RingBufferTCPHandshake struct {
	data [][]*TCPHandshake
	end  int
	size int
}

func (buffer *RingBufferTCPHandshake) Init(size int) {
	buffer.size = size
	buffer.data = make([][]*TCPHandshake, size)
}

func (buffer *RingBufferTCPHandshake) Rotate() {
	next_end := (buffer.end + 1) % buffer.size
	buffer.data[next_end] = make([]*TCPHandshake, 0)
	buffer.end = next_end
}

func (buffer *RingBufferTCPHandshake) Inc(handshake *TCPHandshake) {
	end := buffer.end
	buffer.data[end] = append(buffer.data[end], handshake)

}

func (buffer *RingBufferTCPHandshake) GetAll() []*TCPHandshake {
	var data []*TCPHandshake
	for _, handshakes := range buffer.data {
		data = append(data, handshakes...)
	}
	return data
}

type ProbeTCPHandshake struct {
	ring_buffer                             RingBufferTCPHandshake
	metric_incomplete_handshakes_per_minute prometheus.Gauge
}

func (probe *ProbeTCPHandshake) Init() {
	// TODO: Do not use magic constants
	probe.ring_buffer.Init(60 / int(TICK_INTERVAL.Seconds()))
	probe.metric_incomplete_handshakes_per_minute = prometheus.NewGauge(prometheus.GaugeOpts{Name: "doggo_metric_incomplete_tcp_handshakes_per_minute"})
}

func PrintTCP(tcp *layers.TCP) {
	glog.Infof("TCP packet from src port %d to dst port %d, SYN %t, ACK %t, seq %d, ack: %d\n",
		tcp.SrcPort,
		tcp.DstPort,
		tcp.SYN,
		tcp.ACK,
		tcp.Seq,
		tcp.Ack,
	)
}

func (probe *ProbeTCPHandshake) ParseTCPDatagram(tcp *layers.TCP) {
	if tcp.SYN && !tcp.ACK {
		// 1st part of handshake (SYN)
		glog.Infoln("Detected 1st part of TCP handshake")
		PrintTCP(tcp)
		handshake := TCPHandshake{part1: tcp}
		probe.ring_buffer.Inc(&handshake)

	} else if tcp.SYN && tcp.ACK {
		// 2nd part of handshake (SYN+ACK)
		for _, handshake := range probe.ring_buffer.GetAll() {
			if handshake.part1 != nil && handshake.part1.Seq+1 == tcp.Ack {
				glog.Infoln("Detected 2nd part of TCP handshake")
				PrintTCP(tcp)
				handshake.part2 = tcp
			}
		}

	} else if tcp.ACK {
		// 3rd part of handshake (ACK)
		for _, handshake := range probe.ring_buffer.GetAll() {
			if handshake.part2 != nil && handshake.part2.Seq+1 == tcp.Ack {
				glog.Infoln("Detected 3rd part of TCP handshake")
				PrintTCP(tcp)
				handshake.part3 = tcp
			}
		}
	}
}

func (probe *ProbeTCPHandshake) Update() {
	probe.ring_buffer.Rotate()
	handshakes := probe.ring_buffer.GetAll()

	var incomplete_handshakes int
	for _, handshake := range handshakes {
		if handshake.part1 != nil && handshake.part3 == nil {
			incomplete_handshakes++
		}
	}
	if incomplete_handshakes > 20 {
		glog.Warningln(
			incomplete_handshakes,
			"incomplete TCP 3-way handshakes logged (possible TCP SYN port scan or a DoS attack)",
		)
	}
	probe.metric_incomplete_handshakes_per_minute.Set(float64(incomplete_handshakes))
}
