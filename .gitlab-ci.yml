---

stages:
  - test
  - release

image: "quay.io/fedora/fedora:35"

.install:
  before_script:
    - dnf -y install make git pre-commit golang libpcap-devel clang

pre-commit:
  extends: .install
  stage: test
  script: pre-commit run --all-files

build:
  extends: .install
  stage: test
  before_script:
    - dnf install -y make git pre-commit golang libpcap-devel clang
    - dnf install -y wget xz flex bison
    - pushd .
    - |  # Get and compile musl-gcc
      cd && wget https://musl.libc.org/releases/musl-1.2.3.tar.gz
      tar xvf musl-1.2.3.tar.gz 
      cd musl-1.2.3/ && ./configure && make && make install
      export PATH=$PATH:/usr/local/musl/bin/
      cd /usr/local/musl/include
      ln -s /usr/include/linux/ linux
      ln -s /usr/include/asm asm
      ln -s /usr/include/asm-generic/ asm-generic
      ln -s /usr/include/pcap.h pcap.h
      ln -s /usr/include/pcap/ pcap
    - |  # Get and compile libpcap for amd64
      export PCAPV=1.10.1
      cd && wget -q http://www.tcpdump.org/release/libpcap-$PCAPV.tar.gz
      tar xf libpcap-$PCAPV.tar.gz
      mv libpcap-$PCAPV libpcap-$PCAPV-amd64 && cd libpcap-$PCAPV-amd64
      CC=musl-gcc ./configure --with-pcap=linux && make
      cp libpcap.a /usr/local/musl/lib
    - |  # Get arm64 cross compiler
      cd && wget -q https://musl.cc/aarch64-linux-musl-cross.tgz
      tar xvf aarch64-linux-musl-cross.tgz
      export PATH=$PATH:$HOME/aarch64-linux-musl-cross/bin
      export CC_ARM64=aarch64-linux-musl-gcc
      cd aarch64-linux-musl-cross/aarch64-linux-musl/include
      ln -s /usr/include/linux/ linux
      ln -s /usr/include/asm asm
      ln -s /usr/include/asm-generic/ asm-generic
      ln -s /usr/include/pcap.h pcap.h
      ln -s /usr/include/pcap/ pcap
    - |  # Compile libpcap for arm64
      cd && tar xf libpcap-$PCAPV.tar.gz
      mv libpcap-$PCAPV libpcap-$PCAPV-arm64 && cd libpcap-$PCAPV-arm64
      CC=$CC_ARM64 CFLAGS='-Os' ./configure --host=aarch64-unknown-linux-gnu --with-pcap=linux && make
      cp libpcap.a $($CC_ARM64 -print-sysroot)/lib

  script:
    - popd
    - echo BUILD_JOB_ID=$CI_JOB_ID >> build.env
    - make all
  artifacts:
    paths:
      - bin/*
    reports:
      dotenv: build.env

release:
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  rules:
    - if: $CI_COMMIT_TAG
  script: 'echo Creating release $CI_COMMIT_TAG'
  needs:
    - job: build
      artifacts: true
  release:
    name: 'Release $CI_COMMIT_TAG'
    description: 'Release of doggo $CI_COMMIT_TAG'
    tag_name: '$CI_COMMIT_TAG'
    ref: '$CI_COMMIT_TAG'
    assets:
      links:
        - name: 'amd64 executable'
          url: 'https://gitlab.com/testing-farm/doggo/-/jobs/$BUILD_JOB_ID/artifacts/raw/bin/doggo-amd64'
          filepath: '/doggo-amd64'
        - name: 'arm64 executable'
          url: 'https://gitlab.com/testing-farm/doggo/-/jobs/$BUILD_JOB_ID/artifacts/raw/bin/doggo-arm64'
          filepath: '/doggo-arm64'
