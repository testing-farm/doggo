package main

import (
	"github.com/golang/glog"
	"github.com/google/gopacket/layers"
	"github.com/prometheus/client_golang/prometheus"
)

type RingBufferInt struct {
	data []int
	end  int
	size int
}

func (buffer *RingBufferInt) Init(size int) {
	buffer.size = size
	buffer.data = make([]int, size)
}

func (buffer *RingBufferInt) Rotate() {
	next_end := (buffer.end + 1) % buffer.size
	buffer.data[next_end] = 0
	buffer.end = next_end
}

func (buffer *RingBufferInt) Inc(n int) {
	end := buffer.end
	buffer.data[end] += n

}

func (buffer *RingBufferInt) GetAll() int {
	var total_amount int
	for _, buff_count := range buffer.data {
		total_amount += buff_count
	}
	return total_amount
}

type ProbeNetScan struct {
	arp_buffer                 RingBufferInt
	tcp_null_buffer            RingBufferInt
	metric_arp_per_minute      prometheus.Gauge
	metric_tcp_null_per_minute prometheus.Gauge
}

func (probe *ProbeNetScan) ParseTCPDatagram(tcp *layers.TCP) {
	if !tcp.ACK && !tcp.RST && !tcp.SYN {
		probe.tcp_null_buffer.Inc(1)
	}
}

func (probe *ProbeNetScan) Update() {
	probe.arp_buffer.Rotate()
	probe.tcp_null_buffer.Rotate()

	arp_packets := probe.arp_buffer.GetAll()
	tcp_null_packets := probe.tcp_null_buffer.GetAll()

	if arp_packets > 30 {
		glog.Warningln("Suspected network scan:", arp_packets, "ARP packets per minute logged")
	}
	if tcp_null_packets > 30 {
		glog.Warningln("Suspected network scan:", tcp_null_packets, "TCP NULL packets per minute logged")
	}

	probe.metric_arp_per_minute.Set(float64(arp_packets))
	probe.metric_tcp_null_per_minute.Set(float64(tcp_null_packets))
}

func (probe *ProbeNetScan) Init() {
	// Ring buffer stores amount of packets per minute
	buffer_size := 60 / int(TICK_INTERVAL.Seconds())
	probe.arp_buffer.Init(buffer_size)
	probe.tcp_null_buffer.Init(buffer_size)
	probe.metric_arp_per_minute = prometheus.NewGauge(prometheus.GaugeOpts{Name: "doggo_metric_arp_packets_per_minute"})
	probe.metric_tcp_null_per_minute = prometheus.NewGauge(prometheus.GaugeOpts{Name: "doggo_metric_tcp_null_packets_per_minute"})
}
