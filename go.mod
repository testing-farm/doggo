module gitlab.com/testing-farm/doggo

go 1.16

require (
	github.com/cilium/ebpf v0.8.1
	github.com/golang/glog v1.0.0
	github.com/google/gopacket v1.1.19
	github.com/prometheus/client_golang v1.12.1
	github.com/prometheus/procfs v0.7.3
)
