package main

import (
	"os"
	"strconv"

	"github.com/golang/glog"
	"github.com/prometheus/client_golang/prometheus"
)

type ProbeSELinux struct {
	metric_selinux_enabled prometheus.Gauge
}

func (probe *ProbeSELinux) Update() {
	file, err := os.ReadFile("/sys/fs/selinux/enforce")
	if err != nil {
		glog.Fatal(err)
	}
	file_int, err := strconv.Atoi(string(file))
	if err != nil {
		glog.Fatal(err)
	}
	if file_int == 0 {
		glog.Warningln("Detected that SELinux is disabled")
	}
	probe.metric_selinux_enabled.Set(float64(file_int))
}

func (probe *ProbeSELinux) Init() {
	probe.metric_selinux_enabled = prometheus.NewGauge(prometheus.GaugeOpts{Name: "doggo_metric_selinux_enabled"})
}
