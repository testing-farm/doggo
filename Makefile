CLANG ?= clang
CFLAGS := -O2 -g -Wall -Werror $(CFLAGS)
CC_AMD64 := musl-gcc
CC_ARM64 := aarch64-linux-musl-gcc

all: generate build

generate: export BPF_CLANG := $(CLANG)
generate: export BPF_CFLAGS := $(CFLAGS)
generate:
	go generate

build-amd64:
	mkdir -p bin && env GOOS=linux GOARCH=amd64 CC=${CC_AMD64} go build --ldflags "-linkmode external -extldflags '-static'" -o bin/doggo-amd64

build-arm64:
	mkdir -p bin && env GOOS=linux GOARCH=arm64 CGO_ENABLED=1 CC=${CC_ARM64} go build --ldflags "-linkmode external -extldflags '-static'" -o bin/doggo-arm64

build: build-amd64 build-arm64

clean:
	rm -rf bin/
