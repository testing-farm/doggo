package main

import (
	"flag"
	"math"
	"net"
	"os"
	"strings"
	"sync"
	"time"

	"github.com/golang/glog"
	"github.com/google/gopacket"
	"github.com/google/gopacket/layers"
	"github.com/google/gopacket/pcap"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/push"
	"github.com/prometheus/procfs"
)

const TICK_INTERVAL = 5 * time.Second

var mutex sync.Mutex // Global Lock for the whole program

func GetLoadAvg() *procfs.LoadAvg {
	fs, err := procfs.NewDefaultFS()
	if err != nil {
		glog.Fatal(err)
	}
	avg, err := fs.LoadAvg()
	if err != nil {
		glog.Fatal(err)
	}
	return avg
}

func GetCPUTotal() procfs.CPUStat {
	fs, err := procfs.NewDefaultFS()
	if err != nil {
		glog.Fatal(err)
	}
	stats, err := fs.Stat()
	if err != nil {
		glog.Fatal(err)
	}
	return stats.CPUTotal
}

func GetTotalNetworkStats() procfs.NetDevLine {
	fs, err := procfs.NewDefaultFS()
	if err != nil {
		glog.Fatal(err)
	}
	net_dev, err := fs.NetDev()
	if err != nil {
		glog.Fatal(err)
	}
	return net_dev.Total()
}

func GetNetworkInterfaces() []net.Interface {
	interfaces, err := net.Interfaces()
	if err != nil {
		glog.Fatal(err)
	}
	return interfaces
}

func GetLocalIPv4Addresses() []net.IP {
	host, _ := os.Hostname()
	addrs, _ := net.LookupIP(host)
	ipv4s := []net.IP{net.IPv4(127, 0, 0, 1)}
	for _, addr := range addrs {
		if ipv4 := addr.To4(); ipv4 != nil {
			glog.Info("IPv4: ", ipv4)
			ipv4s = append(ipv4s, ipv4)
		}
	}
	return ipv4s
}

func StartPacketFilter(probe_net_scan *ProbeNetScan, probe_tcp_handshake *ProbeTCPHandshake, probe_udp *ProbeUDP) {
	glog.Info("Packet filter starting")

	var handlers []*pcap.Handle
	buffer := int32(1600)
	filter := "arp or tcp or udp"
	devices := GetNetworkInterfaces()

	for _, device := range devices {
		if device.Name == "lo" {
			glog.Info("skipping ", device.Name, " (loopback)")
			continue
		}
		if !strings.Contains(device.Flags.String(), "up") {
			glog.Info("skipping ", device.Name, " (is down)")
			continue
		}
		glog.Info("Getting handler for interface ", device.Name)
		handler, err := pcap.OpenLive(device.Name, buffer, false, pcap.BlockForever)
		if err != nil {
			glog.Fatal(err)
		}
		defer handler.Close()

		if err := handler.SetBPFFilter(filter); err != nil {
			glog.Info(err)
			continue
		}

		handlers = append(handlers, handler)
	}

	var stops []chan struct{}
	for _, handler := range handlers {
		stop := make(chan struct{})
		go PacketParser(probe_net_scan, probe_tcp_handshake, probe_udp, handler, stop)
		stops = append(stops, stop)
	}

	// Wait forever
	time.Sleep(time.Hour * math.MaxInt16)

	for _, stop := range stops {
		defer close(stop)
	}
}

func PacketParser(probe_net_scan *ProbeNetScan, probe_tcp_handshake *ProbeTCPHandshake, probe_udp *ProbeUDP, handler *pcap.Handle, stop chan struct{}) {
	glog.Info("Started sniffing on an interface")
	source := gopacket.NewPacketSource(handler, handler.LinkType())
	in := source.Packets()
	for {
		var packet gopacket.Packet
		select {
		case <-stop:
			glog.Info("Stopping a sniffer")
			return
		case packet = <-in:
			parsed_layer_arp := packet.Layer(layers.LayerTypeARP)
			if parsed_layer_arp != nil {
				probe_net_scan.arp_buffer.Inc(1)
				continue
			}

			parsed_layer_tcp := packet.Layer(layers.LayerTypeTCP)
			if parsed_layer_tcp != nil {
				tcp, _ := parsed_layer_tcp.(*layers.TCP)
				probe_tcp_handshake.ParseTCPDatagram(tcp)
				probe_net_scan.ParseTCPDatagram(tcp)
				continue
			}

			parsed_layer_udp := packet.Layer(layers.LayerTypeUDP)
			parsed_layer_ip := packet.Layer(layers.LayerTypeIPv4)
			if parsed_layer_udp != nil && parsed_layer_ip != nil {
				udp, _ := parsed_layer_udp.(*layers.UDP)
				ip, _ := parsed_layer_ip.(*layers.IPv4)
				probe_udp.ParseUDPDatagram(udp, ip)
				continue
			}
		}
	}
}

func main() {
	// Parse command line parameters
	pushgateway_url_ptr := flag.String("pushgateway-url", "", "URL to a Prometheus Pushgateway to export metrics.")
	request_id_ptr := flag.String("request-id", "", "Unique identifier of the testing request.")
	log_debug_ptr := flag.Bool("debug", false, "Print debug outputs.")

	flag.Parse()
	if *pushgateway_url_ptr == "" || *request_id_ptr == "" {
		glog.Fatal("Error: Missing command line parameters. Run ", os.Args[0], " -h to get help.")
	}

	// Init logger
	if *log_debug_ptr {
		flag.Set("stderrthreshold", "INFO")
	} else {
		flag.Set("stderrthreshold", "WARNING")
	}

	metric_load_avg_1 := prometheus.NewGauge(prometheus.GaugeOpts{Name: "doggo_metric_load_avg_1"})
	metric_load_avg_5 := prometheus.NewGauge(prometheus.GaugeOpts{Name: "doggo_metric_load_avg_5"})
	metric_load_avg_15 := prometheus.NewGauge(prometheus.GaugeOpts{Name: "doggo_metric_load_avg_15"})
	metric_cpu_time_user := prometheus.NewGauge(prometheus.GaugeOpts{Name: "doggo_metric_cpu_time_user"})
	metric_cpu_time_kernel := prometheus.NewGauge(prometheus.GaugeOpts{Name: "doggo_metric_cpu_time_kernel"})
	metric_received_megabytes := prometheus.NewGauge(prometheus.GaugeOpts{Name: "doggo_metric_received_megabytes"})
	metric_transmitted_megabytes := prometheus.NewGauge(prometheus.GaugeOpts{Name: "doggo_metric_transmitted_megabytes"})

	probe_ports := ProbeListeningTCPPorts{}
	probe_ports.Init()

	probe_net_scan := ProbeNetScan{}
	probe_net_scan.Init()

	probe_tcp_handshake := ProbeTCPHandshake{}
	probe_tcp_handshake.Init()

	probe_udp := ProbeUDP{}
	probe_udp.Init()

	probe_kernel_module := ProbeKernelModule{}
	probe_kernel_module.Init()
	defer probe_kernel_module.Destroy()

	probe_selinux := ProbeSELinux{}
	probe_selinux.Init()

	pusher := push.New(*pushgateway_url_ptr, *request_id_ptr)
	pusher.Collector(metric_load_avg_1)
	pusher.Collector(metric_load_avg_5)
	pusher.Collector(metric_load_avg_15)
	pusher.Collector(metric_cpu_time_user)
	pusher.Collector(metric_cpu_time_kernel)
	pusher.Collector(metric_received_megabytes)
	pusher.Collector(metric_transmitted_megabytes)
	pusher.Collector(probe_ports.metric_total_listening_tcp_ports)
	pusher.Collector(probe_ports.metric_new_listening_tcp_ports_since_start)
	pusher.Collector(probe_net_scan.metric_arp_per_minute)
	pusher.Collector(probe_net_scan.metric_tcp_null_per_minute)
	pusher.Collector(probe_tcp_handshake.metric_incomplete_handshakes_per_minute)
	pusher.Collector(probe_udp.metric_unique_target_ips)
	pusher.Collector(probe_udp.metric_max_unique_target_ports)
	pusher.Collector(probe_kernel_module.metric_times_sys_finit_module_called)
	pusher.Collector(probe_selinux.metric_selinux_enabled)

	go StartPacketFilter(&probe_net_scan, &probe_tcp_handshake, &probe_udp)

	for tick := range time.Tick(TICK_INTERVAL) {
		// Fetch metrics and push them to gateway
		load_avg := GetLoadAvg()
		metric_load_avg_1.Set(load_avg.Load1)
		metric_load_avg_5.Set(load_avg.Load5)
		metric_load_avg_15.Set(load_avg.Load15)

		cpu_total := GetCPUTotal()
		metric_cpu_time_user.Set(cpu_total.User)
		metric_cpu_time_kernel.Set(cpu_total.System)

		network_stats := GetTotalNetworkStats()
		metric_received_megabytes.Set(float64(network_stats.RxBytes / 1000 / 1000))
		metric_transmitted_megabytes.Set(float64(network_stats.TxBytes / 1000 / 1000))

		pusher.Push()

		// Update probes
		probe_ports.Update()
		probe_net_scan.Update()
		probe_tcp_handshake.Update()
		probe_udp.Update()
		probe_kernel_module.Update()
		probe_selinux.Update()

		// Debug prints
		glog.Info("Captured arp packets in the last minute:", probe_net_scan.arp_buffer)
		glog.Info("Listening ports:", probe_ports.listening_ports, "and their inodes:", probe_ports.inodes)
		glog.Info("UDPs:", probe_udp.udp_map)
		glog.Info(tick)
	}
}
