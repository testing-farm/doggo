# Doggo

"Guard Dog for Testing Farm written in Go" - `doggo` is a monitoring tool created for Testing Farm test environments to monitor system usage. The tool exports metrics about the system to a Prometheus Pushgateway.

## Installation
For released executable binaries, see [Releases](https://gitlab.com/testing-farm/doggo/-/releases) page.

## Usage
Privileged user access is required in order to run it. Run the tool as: `doggo -pushgateway-url URL -request-id ID`, where
* `URL` is a url to a Prometheus Pushgateway, and
* `ID` is a unique label within Prometheus to distinguish metrics from different test environments.
