package main

import (
	"encoding/binary"
	"net"

	"github.com/golang/glog"
	"github.com/google/gopacket/layers"
	"github.com/prometheus/client_golang/prometheus"
)

func ip_to_uint32(ip net.IP) uint32 {
	return binary.BigEndian.Uint32(ip)
}

func uint32_to_ip(ip uint32) net.IP {
	ip_bytes := make([]byte, 4)
	for i := uint32(0); i < 4; i++ {
		ip_bytes[3-i] = byte((ip >> (8 * i)) & 0xff)
	}
	return ip_bytes
}

type ProbeUDP struct {
	udp_map                        map[uint32](map[layers.UDPPort]bool)
	local_ipv4s                    map[uint32]bool
	metric_unique_target_ips       prometheus.Gauge
	metric_max_unique_target_ports prometheus.Gauge
}

func (probe *ProbeUDP) Init() {
	probe.udp_map = make(map[uint32]map[layers.UDPPort]bool)
	probe.local_ipv4s = make(map[uint32]bool)
	local_ipv4s := GetLocalIPv4Addresses()
	for _, ip := range local_ipv4s {
		probe.local_ipv4s[ip_to_uint32(ip)] = true
	}
	probe.metric_unique_target_ips = prometheus.NewGauge(prometheus.GaugeOpts{Name: "doggo_metric_udp_unique_target_ips"})
	probe.metric_max_unique_target_ports = prometheus.NewGauge(prometheus.GaugeOpts{Name: "doggo_metric_udp_max_unique_target_ports"})

}

func PrintIPv4(ip *layers.IPv4) {
	glog.Infof("IPv4 packet from %s to %s\n",
		ip.SrcIP.String(),
		ip.DstIP.String(),
	)
}

func PrintUDP(udp *layers.UDP) {
	glog.Infof("UDP packet from src port %d to dst port %d\n",
		udp.SrcPort,
		udp.DstPort,
	)
}

func (probe *ProbeUDP) ParseUDPDatagram(udp *layers.UDP, ip *layers.IPv4) {
	ip_int := ip_to_uint32(ip.DstIP)
	if _, ok := probe.local_ipv4s[ip_int]; ok {
		return
	}

	mutex.Lock()
	if _, ok := probe.udp_map[ip_int]; !ok {
		probe.udp_map[ip_int] = make(map[layers.UDPPort]bool)
	}
	probe.udp_map[ip_int][udp.DstPort] = true
	mutex.Unlock()
}

func (probe *ProbeUDP) Update() {
	var unique_ip_addresses int
	var max_unique_ports int

	for ip, ports_map := range probe.udp_map {
		unique_ip_addresses++
		if len(ports_map) > max_unique_ports {
			max_unique_ports = len(ports_map)
		}
		if len(ports_map) > 30 {
			glog.Warningln(
				"Suspected network scan: for",
				uint32_to_ip(ip),
				"IP address, UDP packets were sent to",
				len(ports_map),
				"unique ports",
			)
		}
	}
	probe.metric_unique_target_ips.Set(float64(unique_ip_addresses))
	probe.metric_max_unique_target_ports.Set(float64(max_unique_ports))
}
